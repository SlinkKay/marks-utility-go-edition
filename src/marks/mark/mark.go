package main

import (
    "os"
    "fmt"
    "marks/shared"
)

func main() {

    // get the value that was passed in
    args, err := shared.GetArgs()

    if err != nil {
        fmt.Println("No shortcut name given")
        return
    }

    shortcutName = args[0]

    // get the current directory

    dir, _ := os.Getwd()

    fmt.Printf("shortcut: %s Directory: %s\n", shortcutName, dir)

    // make sure that we can find the .marks directory

    marksDir, err := shared.getMarksDirectory()

    if (err != nil) {
        fmt.Println(".marks directory could not be found")
        return
    }

    fmt.Println(marksDir)

    // create a symbolic link from the .marks directory using the shortcut
    // to the current directory

    os.Symlink(dir, marksDir+"/"+shortcutName)
}
