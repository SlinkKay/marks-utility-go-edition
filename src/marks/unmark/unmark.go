package main

import (
    "marks/shared"
    "fmt"
    "os"
)


func main() {
    // make sure that something was passed in
    args, err := shared.GetArgs()

    if err != nil {
        fmt.Println("No shortcut given")
        return
    }

    // cycle though the shortcuts 
    for index, _ := range args {
        // check to see if that shortcut exists in the list
        if existsInList(args[index]){
            //  if so remove item from the list
            removeItem(args[index])
        } else {
            // if not display an error
            fmt.Println("Item was not marked")
        }

        
    }
}

func removeItem(item string){
    directory, _ := shared.GetMarksDirectory()
    os.Remove(directory+"/"+item)
}

func existsInList(item string) bool {
    directory, _ := shared.GetMarksDirectory()

    exists,_ := shared.Exists(directory+"/"+item)

    return exists
}