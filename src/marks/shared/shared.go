package shared

import (
    "os"
    "os/user"
    "errors"
)

func GetMarksDirectory() (string, error) {
    home := getHome()

    directory := home + "/.marks"
    exists, err := Exists(directory)

    if !exists {
        return "", err
    }

    return directory, nil
}

func Exists(path string) (bool, error) {
    _, err := os.Stat(path)
    if err == nil { return true, nil }
    if os.IsNotExist(err) { return false, nil }
    return false, err
}

func getHome() string {
    user, _ := user.Current()

    return user.HomeDir
}

func GetArgs() ([]string, error) {
    args := os.Args[1:] 

    if len(args) == 0 {
        return nil, errors.New("No Arguments")
    }

    return args, nil
}