package main

import (
    "fmt"
    "os/exec"
    "log"
    "strings"
    "marks/shared"
)

func main() {

    // check to see if the home path is valid or real
    directory, err := shared.GetMarksDirectory()

    if (err != nil) {
        fmt.Println(".marks directory could not be found")
        return
    }

    // TODO detect to see if the ls command is executeable

    out, err := exec.Command("ls", "-l", directory).Output()
    if err != nil {
        log.Fatal(err)
    }
    // TODO find a better way to do this
    // Hack to convert to a string
    newOut := fmt.Sprintf("%s", out)
    aItems := getMarks(newOut)
    for index, _ := range aItems {
        fmt.Printf("    %s\n", aItems[index])
    }
}

func getHome() string{
    out, err := exec.Command("sh", "-c","echo ~").Output()
    if err != nil {
        log.Fatal(err)
    }

    return strings.Trim(fmt.Sprintf("%s",out),"\n")
}

func getMarks(lsItem string) []string{
    var slice []string
    // Split the ls into other items
    aOutput := strings.Split(lsItem,"\n")
    // For each item in the split find the relevant data
    for index, _ := range aOutput {
        aItemSplit := strings.Split(aOutput[index], " ")
        for index,_ := range aItemSplit {
            // If the item is an empty string then move on
            if (aItemSplit[index] == ""){
                continue
            } 
                        
            var previous string
            var current string
            current = aItemSplit[index];
            if index == 0{
                previous = aItemSplit[index]
            } else {
                previous = aItemSplit[index-1]
            }
            // If the current split is a -> then the previous and next are what we want
            if current == "->" {
                aItem := fmt.Sprintf("%s %s %s\n", previous, current, aItemSplit[index+1])
                aItem = strings.Trim(aItem, "\n")
                //fmt.Println(aItem)
                slice = append(slice, aItem)
            }
            
        } 
    }
    return slice
}
